package BD;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.LinkedList;

import Controller.CConsole;

public class Xml_to_BD {
	public static Connection conn = null;
	static String user = "root";
	static String pass = "";
	static String url = "jdbc:mysql://localhost:3306";

	public static void Create_BD(String BD) {

		// Creamos la Conexion sin BD
		try {
			if (Xml_to_BD.conn == null || conn.isClosed()) {
				Class.forName("com.mysql.jdbc.Driver");
				Xml_to_BD.conn = DriverManager.getConnection(url, user, pass);
			}
		} catch (Exception e) {
			CConsole.ConsoleFrame.ConsoleArea.append("\n "+e.getMessage());
		}

		// creamos la BD y volvemos abrir conexion con BD
		try {
			Statement stmt = Xml_to_BD.conn.createStatement();

			String sql = "CREATE DATABASE if not exists " + BD;

			stmt.executeUpdate(sql);

			Xml_to_BD.conn.close();
			Xml_to_BD.conn = null;

			CConsole.ConsoleFrame.ConsoleArea.append("\n" + sql);
			Xml_to_BD.Open(BD);
		} catch (SQLException e) {
			CConsole.ConsoleFrame.ConsoleArea.append("\n "+e.getMessage());
		}
	}

	// metodo que abre una conexion con DB specificada
	public static void Open(String BD) {
		try {
			if (Xml_to_BD.conn == null || conn.isClosed()) {
				Class.forName("com.mysql.jdbc.Driver");
				Xml_to_BD.conn = DriverManager.getConnection(Xml_to_BD.url + "/" + BD, Xml_to_BD.user, Xml_to_BD.pass);
			}
		} catch (Exception e) {
			CConsole.ConsoleFrame.ConsoleArea.append("\n "+e.getMessage());
		}
	}

	public static void Create_Table(String name, LinkedList<String> campos, LinkedList<String> valores) {

		String Create_tabel = "CREATE TABLE if not exists " + name + "(";

		for (String camp : campos) {
			
			//si es id  anadimos primary key
			if(camp.equals("id"))
				Create_tabel += camp + " varchar(45) not null PRIMARY KEY";
			else
				Create_tabel += camp + " varchar(45) not null";
			
			if (!camp.equals(campos.getLast())) {
				Create_tabel += ",";
			} else {
				Create_tabel += ");";
			}
		}

		try {
			Statement stmt = conn.createStatement();

			stmt.executeUpdate(Create_tabel);

		} catch (SQLException e) {
			CConsole.ConsoleFrame.ConsoleArea.append("\n "+e.getMessage());
		}

		// insertamos valores
		Xml_to_BD.insert_value(name, valores);

	}

	// metodo que nos inserta valores
	public static void insert_value(String name, LinkedList<String> valores) {

		String Insert = "INSERT INTO " + name + " VALUES (";

		for (int i=0;i<valores.size();i++) {
			Insert += "'" + valores.get(i) + "'";

			if (i!=valores.size()-1) {
				Insert += ",";
			} else {
				Insert += ");";
			}
		}

		CConsole.ConsoleFrame.ConsoleArea.append("\n" + Insert);

		Statement stmt;
		try {
			stmt = conn.createStatement();
			stmt.executeUpdate(Insert);
		} catch (SQLException e) {
			CConsole.ConsoleFrame.ConsoleArea.append("\n "+e.getMessage());
		}

	}

	public static LinkedList<String> getTable(String DB) {
		Open(DB);
		LinkedList<String> tables = new LinkedList<>();
		try {
			DatabaseMetaData md = conn.getMetaData();
			ResultSet rs = md.getTables(null, null, "%", null);
			while (rs.next()) {
				tables.add(rs.getString(3));
			}
		} catch (Exception e) {
			CConsole.ConsoleFrame.ConsoleArea.append("\n "+e.getMessage());
		}

		return tables;
	}

	public static LinkedList<String> getColumWithVal(String table) {
		LinkedList<String> colums = new LinkedList<>();
		LinkedList<String> colums_value = new LinkedList<>();
		Statement statement;
		try {
			// Create a result set
			statement = conn.createStatement();
			ResultSet results = statement.executeQuery("SELECT * FROM " + table);

			// Get resultset metadata
			ResultSetMetaData metadata = results.getMetaData();
			int columnCount = metadata.getColumnCount();

			// Get the column names; column indices start from 1
			for (int i = 1; i <= columnCount; i++) {
				String columnName = metadata.getColumnName(i);
				colums.add(columnName);
			}

			// get the values
			while (results.next()) {
				String lane = "";

				for (String col : colums) {
					lane += col + ":" + results.getString(col);

					if (!col.equals(colums.getLast()))
						lane += ",";
				}

				colums_value.add(lane);
			}

		} catch (SQLException e) {
			CConsole.ConsoleFrame.ConsoleArea.append("\n error: "+e.getMessage());
		}
		
		return colums_value;
	}

}
