package BD;

import java.util.LinkedList;

public class Format_List {
	
	//format to create a DB
	public static void format_to_DB(LinkedList<String> BD_list) {		
		
		Xml_to_BD.Create_BD(BD_list.getFirst());
		
		BD_list.removeFirst();
		
		//boolean tabla = false;
		LinkedList<String> campos = new LinkedList<>();
		LinkedList<String> valores = new LinkedList<>();
		String name_table = null;
		
		for(int i=0;i<BD_list.size();i++) {

			if(!BD_list.get(i).contains(":")) {
				name_table = BD_list.get(i);
			}else {
				String camp = BD_list.get(i).split(":")[0];
				String valor = BD_list.get(i).split(":")[1];
				
				campos.add(camp);
				valores.add(valor);
			}
			
			if(i+1==BD_list.size() || !BD_list.get(i+1).contains(":")){
				Xml_to_BD.Create_Table(name_table, campos, valores);
				campos.clear();
				valores.clear();
			}
		}
	}
	
	public static LinkedList<String> format_to_xml(String list){
		LinkedList<String> listTemp = new LinkedList<>();
		
		String[] arr = list.split(",");
		
		for(String lane:arr) {
			listTemp.add(lane);
		}
		
		return listTemp;
	}
	
	
	
	

}
