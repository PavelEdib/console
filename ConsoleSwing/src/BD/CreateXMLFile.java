package BD;

import java.io.File;
import java.util.LinkedList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import Controller.CConsole;

public class CreateXMLFile {;
	public File path;
	private Element root;
	private Document document;

	public CreateXMLFile(String root,String path) {
		this.path = new File(path);
		Create_Root(root);
	}

	private void Create_Root(String root) {
		try {
			DocumentBuilderFactory documentFactory = DocumentBuilderFactory.newInstance();

			DocumentBuilder documentBuilder = documentFactory.newDocumentBuilder();

			this.document = documentBuilder.newDocument();

			// root element
			this.root = document.createElement(root);
			document.appendChild(this.root);
			
			
			
		} catch (Exception e) {
			CConsole.ConsoleFrame.ConsoleArea.append("\n "+e.getMessage());
		}
	}
	
	public void CreatBlockElement(String elBlock,LinkedList<String> Elements) {
		try {

			// employee element
            Element elementBlock = document.createElement(elBlock);
            this.root.appendChild(elementBlock);
            
            
            //set an attribute to staff element
            Attr attr = document.createAttribute(Elements.getFirst().split(":")[0]);
            attr.setValue(Elements.getFirst().split(":")[1]);
            elementBlock.setAttributeNode(attr);
            Elements.removeFirst();
            
            for(String lane:Elements) {
            	// ADD an element
                Element firstName = document.createElement(lane.split(":")[0]);
                firstName.appendChild(document.createTextNode(lane.split(":")[1]));
                elementBlock.appendChild(firstName);
            }
            
			
			
		} catch (Exception e) {
			CConsole.ConsoleFrame.ConsoleArea.append("\n "+e.getMessage());
		}
	}
	
	public void CreateDoc() {
		try {
		// create the xml file
        //transform the DOM Object to an XML File
        TransformerFactory transformerFactory = TransformerFactory.newInstance();
        Transformer transformer = transformerFactory.newTransformer();
        DOMSource domSource = new DOMSource(document);
        StreamResult streamResult = new StreamResult(this.path);

        // If you use
        // StreamResult result = new StreamResult(System.out);
        // the output will be pushed to the standard output ...
        // You can use that for debugging 

        transformer.transform(domSource, streamResult);
		}catch (Exception e) {
			CConsole.ConsoleFrame.ConsoleArea.append("\n "+e.getMessage());
		}
	}

}
