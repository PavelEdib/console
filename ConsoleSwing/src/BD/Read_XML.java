package BD;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;


public class Read_XML {
	
	static int count = 0;
	static String elementParent;
	static public  LinkedList<String> XML_BD = new LinkedList<String>();

	
    public static void Reader(String path) throws ParserConfigurationException, SAXException, IOException, VerifyError {

        SAXParserFactory saxDoc = SAXParserFactory.newInstance();
        SAXParser saxParser = saxDoc.newSAXParser();

        DefaultHandler handler = new DefaultHandler(){
            String tmpElementName = null;
            String tmpElementValue = null;
            Map<String,String> tmpAtrb=null;

			@Override
            public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
                tmpElementValue = "";
                tmpElementName = qName;
                tmpAtrb=new HashMap<String, String>();
                
                //para conseguir nodo child despuies de nodo raiz
                elementParent = qName;
               
                
                //nos ensena nodo child del parent Raiz
                if(qName.equals(elementParent)) {
                	XML_BD.add(qName);
                	
                }
                
                //nos rellena un map de atributes si tenemos mas
                for (int i=0; i<attributes.getLength(); i++) {
                	if(qName.equals(elementParent)) {
                		XML_BD.add(attributes.getLocalName(i)+":"+attributes.getValue(i));
                    }
                    String aname = attributes.getLocalName(i);
                    String value = attributes.getValue(i);
                    tmpAtrb.put(aname, value);
                }
                
            }

            @Override
            public void endElement(String uri, String localName, String qName)  throws SAXException {
            	
                if(tmpElementName.equals(qName)){
                	if(XML_BD.getLast().equals(tmpElementName)) {
                		XML_BD.removeLast();
                	}
                	
                	XML_BD.add(tmpElementName+":"+tmpElementValue);

                	//Recogemos atributos den la Mapa 
                    for (Map.Entry<String, String> entrySet : tmpAtrb.entrySet()) {
                    	XML_BD.add(entrySet.getKey() + ":"+ entrySet.getValue());
                    }
                }
                
            }

            @Override
            public void characters(char ch[], int start, int length) throws SAXException {
            	//atribuimos el valor
                tmpElementValue = new String(ch, start, length);

            }
        };
      
        
        saxParser.parse(new File(path), handler);
    }
}
