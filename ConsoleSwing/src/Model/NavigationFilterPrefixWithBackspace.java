package Model;

import java.awt.event.*;
import javax.swing.*;
import javax.swing.text.*;

public class NavigationFilterPrefixWithBackspace extends NavigationFilter {
	private int prefixLength;
	private Action deletePrevious;

	public NavigationFilterPrefixWithBackspace(int prefixLength, JTextField component) {
		this.prefixLength = prefixLength;
		deletePrevious = component.getActionMap().get("delete-previous");
		component.getActionMap().put("delete-previous", new BackspaceAction());
		component.setCaretPosition(prefixLength);
	}

	@Override
	public void setDot(NavigationFilter.FilterBypass fb, int dot, Position.Bias bias) {
		fb.setDot(Math.max(dot, prefixLength), bias);
	}

	@Override
	public void moveDot(NavigationFilter.FilterBypass fb, int dot, Position.Bias bias) {
		fb.moveDot(Math.max(dot, prefixLength), bias);
	}

	class BackspaceAction extends AbstractAction {
		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;

		@Override
		public void actionPerformed(ActionEvent e) {
			JTextField component = (JTextField) e.getSource();

			if (component.getCaretPosition() > prefixLength) {
				deletePrevious.actionPerformed(null);
			}
		}
	}

}
