package Model;

import java.io.IOException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;


public class DephLvl {

	public static int level;

	public static int instance(String path) throws ParserConfigurationException, SAXException, IOException {
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		DocumentBuilder db = dbf.newDocumentBuilder();
		Document dom = db.parse(path);
		Element elem = dom.getDocumentElement();
		level = 0;
		// CConsole.ConsoleFrame.ConsoleArea.append("\n "+elem.getNodeName() + "--->" +
		// level);
		NodeList nl = elem.getChildNodes();
		displayLevel(nl, level);
		return level;
	}

	/**
	 * Recursive function to go through the nodes to display the level.
	 * 
	 * @param nl
	 * @param level
	 */
	private static void displayLevel(NodeList nl, int level) {
		level++;
		if (nl != null && nl.getLength() > 0) {
			for (int i = 0; i < nl.getLength(); i++) {
				Node n = nl.item(i);
				if (n.getNodeType() == Node.ELEMENT_NODE) {
					// CConsole.ConsoleFrame.ConsoleArea.append("\n "+n.getNodeName() + "--->" + level);
					displayLevel(n.getChildNodes(), level);
					
					 // how depth is it?
		            if (level > DephLvl.level) {
		            	DephLvl.level = level;
		            }
				}
			}
		} else {
			return;
		}

	}
}
