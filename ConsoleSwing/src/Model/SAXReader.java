package Model;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import Controller.CConsole;


public class SAXReader {
		
	static int count = 0;
	static String elementParent;
	static String Space = "";
	static int countSpace = 3;
	
    public static void Reader(String path,boolean etiqueta) throws ParserConfigurationException, SAXException, IOException, VerifyError {

        SAXParserFactory saxDoc = SAXParserFactory.newInstance();
        SAXParser saxParser = saxDoc.newSAXParser();

        DefaultHandler handler = new DefaultHandler(){
            String tmpElementName = null;
            String tmpElementValue = null;
            Map<String,String> tmpAtrb=null;

			@Override
            public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
            	count++;
                tmpElementValue = "";
                tmpElementName = qName;
                tmpAtrb=new HashMap<String, String>();
                
                //para conseguir nodo child despuies de nodo raiz
                if(count ==2) {
                	elementParent = qName;
                }
                
                //nos ensena nodo child del parent Raiz
                if(qName.equals(elementParent)) {
                	if(etiqueta) CConsole.ConsoleFrame.ConsoleArea.append(qName+" ");
                	else CConsole.ConsoleFrame.ConsoleArea.append("\n ");
                }
                
                //nos rellena un map de atributes si tenemos mas
                for (int i=0; i<attributes.getLength(); i++) {
                	if(qName.equals(elementParent)) {
                		CConsole.ConsoleFrame.ConsoleArea.append("\n "+attributes.getLocalName(i)+" = "+attributes.getValue(i)+" ");
                    }
                    String aname = attributes.getLocalName(i);
                    String value = attributes.getValue(i);
                    tmpAtrb.put(aname, value);
                }
                
                //serve para espacios en sangria
                countSpace+=3;
            }

            @Override
            public void endElement(String uri, String localName, String qName)  throws SAXException {
            	Space="";
            	
            	//nos cuenta los spacios para sangria 
            	for(int i=0;i<countSpace;i++) {
            		Space+=" ";
            	}
            	
            	
                if(tmpElementName.equals(qName)){
                	//opcion de hacer la sangria (con o sin Etiquetas)
                	if(etiqueta) CConsole.ConsoleFrame.ConsoleArea.append("\n"+Space+tmpElementName+" : ");
                	else CConsole.ConsoleFrame.ConsoleArea.append("\n"+Space+" ");
             
                
                	//Recogemos atributos den la Mapa 
                    for (Map.Entry<String, String> entrySet : tmpAtrb.entrySet()) {
                    	CConsole.ConsoleFrame.ConsoleArea.append(entrySet.getKey() + " = "+ entrySet.getValue());
                    }
                    CConsole.ConsoleFrame.ConsoleArea.append(tmpElementValue);
                }
                //nos quita los espacios
                countSpace-=3;
                
            }

            @Override
            public void characters(char ch[], int start, int length) throws SAXException {
            	//atribuimos el valor
                tmpElementValue = new String(ch, start, length);

            }
        };
      
        
        saxParser.parse(new File(path), handler);
    }
}
