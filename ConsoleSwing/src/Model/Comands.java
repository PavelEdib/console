package Model;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.SimpleDateFormat;
import java.util.LinkedList;
import java.util.Locale;

import javax.swing.JOptionPane;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.NodeList;

import Controller.CConsole;

public class Comands {
	public static String path_original;

	// Metod to move a file
	public static boolean mueve(String rutaOrigen, String rutaDestino) {

		boolean result = copia(rutaOrigen, rutaDestino);

		File src = new File(rutaOrigen);
		src.delete();

		return result;

	}

	// Metod to copy a file
	public static boolean copia(String rutaOrigen, String rutaDestino) {

		File src = new File(rutaOrigen);
		File dest = new File(rutaDestino);

		// Exception wrong file
		if (!src.isFile()) {
			CConsole.ConsoleFrame.ConsoleArea.append("\n Wrong file nr 1!");
			return false;
		}

		// Exception wrong directory
		if (!dest.isDirectory()) {
			CConsole.ConsoleFrame.ConsoleArea.append("\n Wrong Directory! Will be created this directory!");
			boolean op = dest.mkdir();

			if (!op) {
				return false;
			}
		}

		// Copy File
		try {
			dest = new File(rutaDestino + "/" + src.getName());

			InputStream is = new FileInputStream(src);
			OutputStream os = new FileOutputStream(dest);

			// buffer size 1K
			byte[] buf = new byte[1024];

			int bytesRead;
			while ((bytesRead = is.read(buf)) > 0) {
				os.write(buf, 0, bytesRead);
			}
			is.close();
			os.close();

		} catch (Exception e) {
			e.printStackTrace();
		}

		return true;

	}

	// Metod to delete a File
	public static boolean elimina(String rutaFichero) {

		File file = new File(rutaFichero);

		// comprobamos si el fichero es corecto
		if (!file.isFile()) {
			CConsole.ConsoleFrame.ConsoleArea.append("Wrong File!!!");
			return false;
		}

		boolean verification = false;

		if (!file.canRead()) {

			int a = JOptionPane.showConfirmDialog(null, file.getName() + " tiene permisos de s�lo lectura\r\n"
					+ "\nAun as�, desea eliminar " + file.getName());

			if (a == JOptionPane.YES_OPTION) {
				verification = true;
			}

			if (verification)
				file.setReadable(true);

		} else {

			int a = JOptionPane.showConfirmDialog(null,"Desea eliminar " + file.getName());

			if (a == JOptionPane.YES_OPTION) {
				verification = true;
			}

		}

		if (verification)
			file.delete();

		return true;

	}



	// Return content from Directory
	public static boolean lista(String rutaDirectory) {

		File directory = new File(rutaDirectory);

		// comprobamos si ruta introdusa es un directory
		if (!directory.isDirectory()) {
			CConsole.ConsoleFrame.ConsoleArea.append("\n Isn't a Directory!");
			return false;
		}

		// format para date
		SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy HH:mm");

		// Recoremos la lista de Files
		for (File file : directory.listFiles()) {
			// si es directory nos ensena de siguente estructura
			if (file.isDirectory()) {
				CConsole.ConsoleFrame.ConsoleArea
						.append("\n" + sdf.format(file.lastModified()) + "   <DIR>             " + file.getName());
			} else {
				// sino de esta estructura
				CConsole.ConsoleFrame.ConsoleArea.append("\n" + sdf.format(file.lastModified()) + "         "
						+ formating(file.length()) + " " + file.getName());
			}
		}

		return true;
	}

	// format a valor bytes de forma 999.999.999
	public static String formating(long value) {
		// damos el formato
		DecimalFormat df = new DecimalFormat("###,###,###");
		df.setDecimalFormatSymbols(new DecimalFormatSymbols(Locale.ITALY));
		String format_value = df.format(value);
		String space = "";
		// metemos spacios a los digitos que nos falta
		for (int i = 0; i < (11 - format_value.length()); i++) {
			space += " ";
		}
		return space + format_value;
	}

	// Return content from Directory with child's directory
	public static boolean listaArbol(String rutaDirectory) {

		File directory = new File(rutaDirectory);

		if (!directory.isDirectory()) {
			CConsole.ConsoleFrame.ConsoleArea.append("\n Isn't a Directory!");
			return false;
		}

		SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy HH:mm");

		// Recoremos la lista de Files
		for (File file : directory.listFiles()) {
			if (file.isDirectory()) {
				CConsole.ConsoleFrame.ConsoleArea.append("\n" + sdf.format(file.lastModified())
						+ "   <DIR>             " + Space(rutaDirectory) + file.getName());
				// llama a si mismo cuando es un directorio
				listaArbol(file.getAbsolutePath());
			} else {
				CConsole.ConsoleFrame.ConsoleArea.append("\n" + sdf.format(file.lastModified()) + "         "
						+ formating(file.length()) + " " + Space(rutaDirectory) + file.getName());
			}
		}

		return true;
	}

	// Nos mete spacios en dependencias donde estamos (directory,subDirectory...)
	public static String Space(String rutaDirectory) {
		String rezult_space = "";

		String[] words_original = path_original.split("/");
		String[] words = rutaDirectory.split("/");

		if (words.length == 1) {
			words = rutaDirectory.split("\\\\");
		}

		int count = words.length - words_original.length;

		for (int i = 0; i < count; i++) {
			rezult_space += "  ";
		}

		return rezult_space;
	}

	// Compare a 2 files txt
	public static void compara(String rutaFichero1, String rutaFichero2) {

		File file1 = new File(rutaFichero1);
		File file2 = new File(rutaFichero2);

		// comprobamos si son fecheros
		if (file1.isFile() && file2.isFile()) {

			String extension1 = file1.getName().split("\\.")[1];
			String extension2 = file2.getName().split("\\.")[1];
			// comprobamos si se acabe con txt
			if (extension1.equals("txt") && extension2.equals("txt")) {
				compare(file1, file2);
			} else {
				CConsole.ConsoleFrame.ConsoleArea.append("\n Wrong Extension!!!");
			}

		} else {
			CConsole.ConsoleFrame.ConsoleArea.append("\n Wrongs Files");
		}

	}

	// segunda parte de comparar
	public static void compare(File file1, File file2) {
		LinkedList<String> array1 = new LinkedList<>();
		LinkedList<String> array2 = new LinkedList<>();

		// Instaciamos los array's con los datos array1=file1 array2=file2
		try {
			FileReader fr1 = new FileReader(file1);
			FileReader fr2 = new FileReader(file2);

			BufferedReader br1 = new BufferedReader(fr1); // creates a buffering character input stream
			BufferedReader br2 = new BufferedReader(fr2);
			String line;

			// rellanamos array con linias din fichero
			while ((line = br1.readLine()) != null) {
				array1.add(line);
			}
			// rellanamos array con linias din fichero
			while ((line = br2.readLine()) != null) {
				array2.add(line);
			}

			br1.close();
			br2.close();

		} catch (Exception e) {
			e.printStackTrace();
		}

		// segunda parte en cual comprobamos si son iguales
		int nr;
		boolean comparar = true;

		// conseguimos mayor nr para nuestro for
		if (array1.size() > array2.size())
			nr = array1.size();
		else
			nr = array2.size();

		for (int i = 0; i < nr; i++) {
			String line1, line2;

			// atribuimos a una linia valor o nada si no existe la linea
			if (array1.size() <= i)
				line1 = "";
			else
				line1 = array1.get(i);

			if (array2.size() <= i)
				line2 = "";
			else
				line2 = array2.get(i);

			// comprobamos si la lineas son iguales
			if (!line1.equals(line2)) {
				comparar = false;
				CConsole.ConsoleFrame.ConsoleArea.append("\n" + file1.getName() + ": " + line1);
				CConsole.ConsoleFrame.ConsoleArea.append("\n" + file2.getName() + ": " + line2);
			}

		}
		// si comparar se quede true son iguales
		if (comparar) {
			CConsole.ConsoleFrame.ConsoleArea.append("\n Ficheror son iguales!!");
		}

	}

	// monstrar un fichero txt
	public static void muestraTXT(String rutaFichero) {

		File file = new File(rutaFichero);
		// comprobamos si es fichero
		if (file.isFile()) {
			// comprobamos si se acaba con TXT
			if (file.getName().split("\\.")[1].equalsIgnoreCase("txt")) {
				CConsole.ConsoleFrame.ConsoleArea.append("\n*****************************************************");
				try {
					BufferedReader br = new BufferedReader(new FileReader(file));
					String line;
					// lee cada linia y nos ensena
					while ((line = br.readLine()) != null) {
						CConsole.ConsoleFrame.ConsoleArea.append("\n" + line);
					}
					br.close();
				} catch (Exception e) {

				}
				CConsole.ConsoleFrame.ConsoleArea.append("\n*****************************************************");
			}
		}

	}

	public static void soloLectura(String rutaFichero, boolean BRead) {
		// creating a new file instance
		File file = new File(rutaFichero);

		if (file.exists()) {

			if (BRead) {
				file.setReadable(true);
			} else {
				file.setReadable(false);
			}
			CConsole.ConsoleFrame.ConsoleArea.append("\n Done!");
		} else
			CConsole.ConsoleFrame.ConsoleArea.append("\n File not found.");
	}

	// Return content from Directory
	public static boolean eliminaCascada(String rutaDirectory, String extension) {

		File directory = new File(rutaDirectory);

		// comprobamos si ruta introdusa es un directory
		if (!directory.isDirectory()) {
			CConsole.ConsoleFrame.ConsoleArea.append("\n Isn't a Directory!");
			return false;
		}

		// Recoremos la lista de Files
		for (File file : directory.listFiles()) {

			// si es directory nos ensena de siguente estructura
			if (!file.isDirectory()) {
				// conseguimos la extension de fichero para comprobarlo
				String tmpEx = file.getName().split("\\.")[1];
				if (tmpEx.equalsIgnoreCase(extension)) {
					CConsole.ConsoleFrame.ConsoleArea.append("\n          " + file.getName());
					Comands.elimina(file.getAbsolutePath());
				}
			}
		}

		return true;
	}
	
	
	public static void cantidadNodo(String nodePath,String nodeName) {
		 
		try { 
			// Defines a factory API that enables applications to obtain a parser that produces DOM object trees from
			// XML documents.
			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
 
			// The Document interface represents the entire HTML or XML document. Conceptually, it is the root of the
			// document tree, and provides the primary access to the document's data.
			Document doc = factory.newDocumentBuilder().parse(nodePath); 

			// Returns a NodeList of all the Elements in document order with a given tag name and are contained in the document.
			NodeList nodes = doc.getElementsByTagName(nodeName);
			CConsole.ConsoleFrame.ConsoleArea.append("\n  Total  of Elements: " + nodes.getLength());
 
		} catch (Exception e) {
			CConsole.ConsoleFrame.ConsoleArea.append("\n "+e.getMessage());
		}
		
	}

}
