package Model;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import Controller.CConsole;

public class BD_Comands {

	public Connection conn = null;
	public String user = "root";
	public String pass = "";
	public String url = "jdbc:mysql://localhost:3306";
	private String BD = null;

	public void sqlQuery(String sqlquery) {

		// Creamos la Conexion sin BD
		//si ya usamos una BD
		if (BD != null)
			Open(this.BD);
		else
			try {
				if (conn == null || conn.isClosed()) {
					Class.forName("com.mysql.jdbc.Driver");
					this.conn = DriverManager.getConnection(url, user, pass);
				}
			} catch (Exception e) {
				CConsole.ConsoleFrame.ConsoleArea.append("\n " + e.getMessage());
			}

		// creamos la BD y volvemos abrir conexion con BD
		try {
			Statement stmt = this.conn.createStatement();

			stmt.executeUpdate(sqlquery);

			this.conn.close();
			this.conn = null;

			CConsole.ConsoleFrame.ConsoleArea.append("\n" + sqlquery);
		} catch (SQLException e) {
			CConsole.ConsoleFrame.ConsoleArea.append("\n " + e.getMessage());
		}
	}

	// metodo que abre una conexion con DB specificada
	public void Open(String BD) {
		try {
			if (this.conn == null || this.conn.isClosed()) {
				Class.forName("com.mysql.jdbc.Driver");
				this.conn = DriverManager.getConnection(this.url + "/" + BD, this.user, this.pass);
			}
		} catch (Exception e) {
			CConsole.ConsoleFrame.ConsoleArea.append("\n " + e.getMessage());
		}
	}
	
	public void setBD(String sqlquery) {
		String BD_f= sqlquery.split(" ")[1];
		this.BD = BD_f.split(";")[0];	
	}
	
	public String getBD() {
		return this.BD;
	}
	
	public void select(String consulta) {
		this.Open(this.BD);
		PreparedStatement stm;
		ResultSet result;

		try {
			stm = this.conn.prepareStatement(consulta);
			result = stm.executeQuery();

			while (result.next()) {
				String lane = "";
				for (int x=1;x<=result.getMetaData().getColumnCount();x++)
				    lane += result.getString(x)+ "  "+this.fillSpace(result.getString(x));
				
				CConsole.ConsoleFrame.ConsoleArea.append("\n"+lane);
			}

		} catch (SQLException e) {
			CConsole.ConsoleFrame.ConsoleArea.append("\n " + e.getMessage());
		}
		
	}
	
	private String fillSpace(String str) {
		String space="";
		
		for(int i=0;i<12-str.length();i++) {
			space+=" "; 
		}
		
		
		return space;
	}
	

}
