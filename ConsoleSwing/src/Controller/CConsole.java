package Controller;

import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.IOException;
import java.util.LinkedList;

import javax.xml.parsers.ParserConfigurationException;

import org.xml.sax.SAXException;

import BD.CreateXMLFile;
import BD.Format_List;
import BD.Read_XML;
import BD.Xml_to_BD;
import Model.BD_Comands;
import Model.Comands;
import Model.DephLvl;
import Model.NavigationFilterPrefixWithBackspace;
import Model.SAXReader;
import View.ConsoleView;

public class CConsole {

	public static ConsoleView ConsoleFrame;

	static private String promp = "C:>";
	static private String comand = "";
	static private String path1;
	static private String path2;
	private boolean modoBD = false;

	public CConsole() {
		ConsoleFrame = new ConsoleView();
		ConsoleFrame.setLocationRelativeTo(null);
		ConsoleFrame.ConsoleArea.append("\n \n" + promp);
		ConsoleFrame.txtC.setText(promp);
		ConsoleFrame.txtC.setNavigationFilter(
				new NavigationFilterPrefixWithBackspace(promp.length(), CConsole.ConsoleFrame.txtC));

		ConsoleFrame.txtC.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e) {
				int key = e.getKeyCode();

				if (key == KeyEvent.VK_ENTER) {

					String comand = ConsoleFrame.txtC.getText();

					ConsoleFrame.ConsoleArea.append("\n \n" + comand);

					 String lineComand = DivideComand(comand);
					
					if(lineComand.equals("modoBD")) {
						modoBD = true;
					}else if(lineComand.equals("modoConsola")) {
						modoBD = false;
					}
					
					if(modoBD) {
						CConsole.promp = "MySql>";
						executeDB(lineComand);
					}else {
						// pasamos la linia a otro split "comand,path1 path2"
						SplitLine(lineComand);
						CConsole.promp = "C:>";
						Execute();
					}
					ConsoleFrame.txtC.setText(promp);
				}
			}
		});

	}

	// dividimos hasta ">"
	private String  DivideComand(String comand) {
		String line = " ";
		try {
			String stringDeriv[] = comand.split(">");
			// String prompt = stringDeriv[0];
			line = stringDeriv[1];
		} catch (Exception e) {

		}
		return line;
		
	}

	// nos devide una cadena en comand , path1,path2
	public void SplitLine(String line) {

		String[] arraySplit = line.split(" ");

		try {
			comand = arraySplit[0];
			path1 = arraySplit[1];
			path2 = arraySplit[2];

		} catch (Exception e) {

		}

	}

	public void Execute() {

		switch (comand) {
		case "copia": {
			boolean result = Comands.copia(path1, path2);
			if (result)
				ConsoleFrame.ConsoleArea.append("\n \n" + "Done!");
			break;
		}
		case "mueve": {
			boolean result = Comands.mueve(path1, path2);
			if (result)
				ConsoleFrame.ConsoleArea.append("\n \n" + "Done!");
			break;
		}
		case "elimina": {
			boolean result = Comands.elimina(path1);
			if (result)
				ConsoleFrame.ConsoleArea.append("\n \n" + "Done!");
			break;
		}
		case "lista": {
			Comands.lista(path1);
			break;
		}
		case "listaArbol": {
			Comands.path_original = path1;
			Comands.listaArbol(path1);
			break;
		}
		case "compara": {
			Comands.compara(path1, path2);
			break;
		}
		case "muestraTXT": {
			Comands.muestraTXT(path1);
			break;
		}
		case "muestraXML": {
			boolean op = false;
			// comprobamos si hemos metido el opcion con o sin etiquetas
			if (path2 != null)
				// si segundo opcion es = /conEtiquetas pasamas a nuestro metodo true si no nos
				// ensena sin Etiquetas
				if (path2.equalsIgnoreCase("/conEtiquetas")) {
					op = true;
				}

			try {
				File file = new File(path1);

				// comprobamos si es un fichero
				if (file.isFile())
					// sacamos el extension y lo comprobamos si es xml
					if (file.getName().split("\\.")[1].equals("xml"))
						SAXReader.Reader(path1, op);
					else
						ConsoleFrame.ConsoleArea.append("\n No es un fichero de tipo xml");

				else
					ConsoleFrame.ConsoleArea.append("\n No es un fichero!!");
			} catch (Exception e) {
				e.printStackTrace();
			}
			break;
		}

		case "soloLectura": {
			if (path1 != null) {
				if (path2 != null) {
					if (path2.equals("/s")) {
						Comands.soloLectura(path1, true);
					} else if (path2.equals("/n")) {
						Comands.soloLectura(path1, false);
					} else {
						ConsoleFrame.ConsoleArea.append("\n Incorect Path!");
					}
				}
			} else {
				ConsoleFrame.ConsoleArea.append("\n The comand must be : soloLectura 	rutaFichero /s || /n");
			}
			break;
		}
		case "eliminaCascada": {
			if (path1 != null && path2 != null) {
				Comands.eliminaCascada(path1, path2);
			} else {
				ConsoleFrame.ConsoleArea.append("\n The comand must be :eliminaCascada  rutaFichero extension");
			}
			break;
		}
		case "nivelXML": {
			if (path1 != null) {
				try {
					int nr = DephLvl.instance(path1);
					ConsoleFrame.ConsoleArea.append("\n Numero de niveles: " + nr);
				} catch (Exception e) {
					ConsoleFrame.ConsoleArea.append("\n " + e.getMessage());
				}
			} else {
				ConsoleFrame.ConsoleArea.append("\n The comand must be :nivelXML			rutaFichero (XML)");
			}
			break;
		}
		case "cantidadNodo": {
			if (path1 != null && path2 != null)
				Comands.cantidadNodo(path1, path2);
			else
				ConsoleFrame.ConsoleArea.append("\n  The comand must be :cantidadNodo 		rutaFichero nombreNodo");
			break;
		}

		case "creaBDdeXML": {
				if(path1!=null) {
					if(path1.split("\\.")[1].equals("xml")) {
						try {
							Read_XML.Reader(path1);
							Format_List.format_to_DB(Read_XML.XML_BD);
							Read_XML.XML_BD.clear();
							ConsoleFrame.ConsoleArea.append("\n Done!");
						} catch (VerifyError | ParserConfigurationException | SAXException | IOException e) {
							ConsoleFrame.ConsoleArea.append("\n " +e.getMessage());
						}	
					}else {
						ConsoleFrame.ConsoleArea.append("\n No es un fichero xml!");
					}
				} else {
					ConsoleFrame.ConsoleArea.append("\n  The comand must be :creaBDdeXML 		rutaFichero");
				}
			break;
		}
		
		case "creaXMLdeBD":{
			if(path1!=null && path2!=null) {
				LinkedList<String> tables = Xml_to_BD.getTable(path1);
				CreateXMLFile xml = new CreateXMLFile(path1, path2);
				
				for(String tab:tables) {
					for(String val:Xml_to_BD.getColumWithVal(tab))
						xml.CreatBlockElement(tab, Format_List.format_to_xml(val));
				}
				xml.CreateDoc();
				ConsoleFrame.ConsoleArea.append("\n Done!");
			}
			break;
		}
		case "modoConsola":{
			ConsoleFrame.ConsoleArea.append("\n Modo consola Activo!");
			break;
		}
		case "help": {
			help();
			break;
		}
		case "exit": {
			ConsoleFrame.dispose();
			break;
		}

		default:
			ConsoleFrame.ConsoleArea.append("\n " + "Uncknown Comand :" + comand);
		}

		comand = "";
		path1 = null;
		path2 = null;

	}
	
	
	public void executeDB(String lane) {
		String first_word = lane.split(" ")[0];
		BD_Comands sql = new BD_Comands();
		if(first_word.equalsIgnoreCase("modoBD")) {
			ConsoleFrame.ConsoleArea.append("\n Modo BD Activo!");
		}else if(first_word.equalsIgnoreCase("select")) {
			sql.select(lane);
		}else if(first_word.equalsIgnoreCase("use")) {
			sql.setBD(lane);
			ConsoleFrame.ConsoleArea.append("\n  Done!");
		}else{
			sql.sqlQuery(lane);
			ConsoleFrame.ConsoleArea.append("\n  Done!");
		}
	}

	public void help() {
		ConsoleFrame.ConsoleArea.append("\n **************Comnands Available*********************");
		ConsoleFrame.ConsoleArea.append("\n  copia    			path1     directory");
		ConsoleFrame.ConsoleArea.append("\n  mueve    			path1     directory");
		ConsoleFrame.ConsoleArea.append("\n  elimina  			path1");
		ConsoleFrame.ConsoleArea.append("\n  lista  	 		          directory");
		ConsoleFrame.ConsoleArea.append("\n  listaArbol     	 		  directory");
		ConsoleFrame.ConsoleArea.append("\n  compara  			fileTXT1  fileTXT2");
		ConsoleFrame.ConsoleArea.append("\n  muestraTXT			fileTXT");
		ConsoleFrame.ConsoleArea.append("\n  muestraXML			fileXML   /conEtiquetas || /sinEtiquetas");
		ConsoleFrame.ConsoleArea.append("\n  soloLectura 		rutaFichero /s || /n");
		ConsoleFrame.ConsoleArea.append("\n  eliminaCascada 	rutaFichero extension");
		ConsoleFrame.ConsoleArea.append("\n  cantidadNodo 		rutaFichero nombreNodo");
		ConsoleFrame.ConsoleArea.append("\n  nivelXML			rutaFichero");
		ConsoleFrame.ConsoleArea.append("\n  creaBDdeXML 		rutaFichero");
		ConsoleFrame.ConsoleArea.append("\n  creaXMLdeBD        nombreBD   FileXML");
		ConsoleFrame.ConsoleArea.append("\n *****************************************************");
	}

}
