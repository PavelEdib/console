package View;

import java.awt.BorderLayout;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import java.awt.Font;

import javax.swing.JTextArea;
import javax.swing.JTextField;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.Insets;

import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;
import javax.swing.JScrollPane;

public class ConsoleView extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public JPanel contentPane;
	public JTextField txtC;
	public JTextArea ConsoleArea;
	private JScrollPane scrollPane;


	/**
	 * Create the frame.
	 */
	public ConsoleView() {
		this.setVisible(true);
		setTitle("JQ System Comand");
		setIconImage(Toolkit.getDefaultToolkit().getImage(ConsoleView.class.getResource("/img/LogoEdib.png")));
		setBackground(Color.BLACK);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setSize(957, 777);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new BorderLayout(0, 0));
		
		ConsoleArea = new JTextArea();
		ConsoleArea.setForeground(Color.GREEN);
		ConsoleArea.setCaretColor(Color.BLACK);
		ConsoleArea.setSelectedTextColor(Color.GREEN);
		ConsoleArea.setLineWrap(false);
		ConsoleArea.setEditable(false);
		ConsoleArea.setFont(new Font("Consolas", Font.BOLD, 14));
		ConsoleArea.setDisabledTextColor(Color.GREEN);
		//ConsoleArea.setSelectedTextColor(Color.GREEN);
		ConsoleArea.setBackground(Color.BLACK);
		ConsoleArea.setText(" Microsoft Windows [Version 10.19329.746] \n  \u00a9 2021 JQ Pavel Corporation. Todos los derechos reservados.");
		
		//contentPane.add(ConsoleArea, BorderLayout.CENTER);
		
	
		
		txtC = new JTextField();
		txtC.setPreferredSize(new Dimension(7, 40));
		txtC.setBorder(new LineBorder(new Color(0, 255, 0), 2));
		txtC.setBackground(Color.BLACK);
		txtC.setForeground(Color.GREEN);
		txtC.setHorizontalAlignment(SwingConstants.LEFT);
		txtC.setFont(new Font("Consolas", Font.BOLD, 14));
		txtC.setText("C:>");
		//txtC.getInputMap().put(KeyStroke.getKeyStroke("BACK_SPACE"), "none");
		txtC.setSelectedTextColor(Color.GREEN);
		txtC.setMargin(new Insets(20, 2, 10, 2));
		contentPane.add(txtC, BorderLayout.SOUTH);
		txtC.setMinimumSize(new Dimension(100,1000));
		
		scrollPane = new JScrollPane(ConsoleArea);
		
		contentPane.add(scrollPane, BorderLayout.CENTER);
		
		
	}



	

}
